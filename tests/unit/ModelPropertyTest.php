<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model\TestUnit;

use Spinit\Core\Model\ModelProperty;
use PHPUnit\Framework\TestCase;
use Spinit\Core\Model\Model;

/**
 * Description of ModelTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ModelPropertyTest extends TestCase
{
    /**
     *
     * @var ModelProperty
     */
    private $object;
    
    /**
     *
     * @var Model
     */
    private $model;
    
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $adapter = new \Spinit\Core\Model\Adapter\Xml\ModelAdapterXml('<model resource="test"><struct><field name="id" type="uuid" pkey="1"/></struct></model>', '');
        $datamanager = \Spinit\Datamanager\DataManagerFactory::getDataManager(CONNECTION_STRING);
        $datamanager->exec('drop table if exists test');
        $datamanager->exec('drop table if exists test__prp');
        $datamanager->exec('drop table if exists test__prpx');
        $datamanager->setParam('userTrace', md5('UserTest'));
        $model = new Model($adapter, $datamanager);
        $model->init();
        $this->model = $model;
        $this->object = new ModelProperty($this->model);
    }
    
    public function testPkey()
    {
    }
    
    public function testResource()
    {
        $this->model->set('id', md5('uno'));
        $this->model->setProperty('hello','ciao uno');
        $this->model->save();
        $this->model->clear();
        
        $this->model->set('id', md5('due'));
        $this->model->setProperty('hello','ciao due');
        $this->model->save();
        $this->model->clear();
        
        $this->model->load(md5('uno'));
        $this->assertEquals(['ciao uno'], $this->model->getProperty('hello'));
        
        $this->model->load(md5('due'));
        $this->assertEquals(['ciao due'], $this->model->getProperty('hello'));
    }
    
    public function testMultipleValue()
    {
        $this->model->load(md5('uno'));
        $this->model->setProperty('hello',['ciao uno', 'ciao tre']);
        $this->model->save();
        
        $this->model->load(md5('uno'));
        $this->assertEquals(['ciao uno', 'ciao tre'], $this->model->getProperty('hello'));
    }
    
    public function testDeleteValue()
    {
        $this->assertEquals(0, $this->model->getDataManager()->load('select count(*) as cc from test__prpx')->first('cc'));
        $this->model->load(md5('uno'));
        $this->model->setProperty('hello',['ciao tre']);        
        $this->model->save();
        
        $this->assertEquals(1, $this->model->getDataManager()->load('select count(*) as cc from test__prpx')->first('cc'));
        
        $this->model->load(md5('uno'));
        $this->assertEquals(['ciao tre'], $this->model->getProperty('hello'));
    }
}