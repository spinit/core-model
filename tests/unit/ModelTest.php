<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model\TestUnit;

use Spinit\Core\Model\Model;
use PHPUnit\Framework\TestCase;

use Spinit\Core\Model\Interfaces\ModelAdapterInterface;
use Spinit\Core\Model\FieldType\ValueType;
use Spinit\Core\Model\FieldType\IncrementType;
use Spinit\Util\Error\NotFoundException;

use Spinit\Datamanager\DataManagerInterface;
use Spinit\Datastruct\DataStructInterface;
use Spinit\Util;

use InvalidArgumentException;
/**
 * Description of ModelTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ModelTest extends TestCase
{
    /**
     *
     * @var Model
     */
    private $object;
    
    public function setUp()
    {
        $this->object = new Model(new AdapterMock(),  new DataManagerMock());
    }
    
    public function testAdapter()
    {
        $this->assertInstanceOf(__NAMESPACE__.'\\AdapterMock', $this->object->getAdapter());
        $this->assertInstanceOf(__NAMESPACE__.'\\DataManagerMock', $this->object->getDataManager());
        $this->assertEquals(3, count($this->object->getFieldList()));
    }
    public function testFieldManipulate()
    {
        $this->assertNotFalse($this->object->has('name'));
        $this->assertFalse($this->object->getField('name')->hasValue());
        $this->object->set('name', 'ok');
        $this->assertTrue($this->object->getField('name')->hasValue());
        $this->assertEquals('ok', $this->object->get('name'));
        $this->object->set('prp.test', 'ok');
        $this->assertEquals('ok', $this->object->get('prp.test'));
    }
    
    /**
     * @expectedException \Spinit\Util\Error\NotFoundException
     */
    public function testFieldNotFoundGet()
    {
        $this->assertFalse($this->object->has('prova'));
        $this->object->get('prova');
    }
    
    /**
     * @expectedException \Spinit\Util\Error\NotFoundException
     */
    public function testFieldNotFoundSet()
    {
        $this->assertFalse($this->object->has('prova'));
        $this->object->set('prova', 'blaaaaaaa');
    }
    
    public function testPkey()
    {
        $this->assertTrue($this->object->has('id')->isPkey());
        $this->assertFalse($this->object->has('pk'));
        
        $this->object->map('pk', 'value')->setPkey();
        $this->assertNotFalse($this->object->has('pk')->isPkey());
        $this->object->getField('pk')->setPkey(false);
        $this->assertFalse($this->object->has('pk')->isPkey());
    }
    public function testLoad()
    {
        $this->object->getDataManager()->setListener('first', function($resource, $pkey) {
            $this->assertEquals('TestTable', $resource);
            $this->assertEquals(['id'=>'1'], $pkey);
            return ['id'=>'1', 'name'=>'test'];
        });
        
        $this->object->load(['id'=>'1']);
        $this->assertEquals('1', $this->object->get('id'));
        $this->assertEquals('test', $this->object->get('name'));
        $this->assertFalse($this->object->getField('name')->isModified());
        $this->object->set('name', 'prova');
        $this->assertTrue($this->object->getField('name')->isModified());
    }
    
    public function testSaveInsert()
    {
        $this->object->getDataManager()->setListener('insert', function($resource, $data) {
            $this->assertEquals('TestTable', $resource['name']);
            $this->assertEquals(['id'=>1, 'name'=>'test'], (array) $data);
        });
        
        $this->object->getDataManager()->setListener('first', function($resource, $pkey) {
            return $pkey;
        });
        
        $this->object->getDataManager()->setListener('update', function($resource, $data, $pkey) {
            $this->assertEquals('TestTable', $resource['name']);
            $this->assertEquals(['id'=>1, 'name'=>'test', 'prp'=>json_encode(['test'=>'ok'])], (array) $data);
            $this->assertEquals(['id'=>1], (array) $pkey);
        });
        
        $this->object->set('name', 'test');
        $this->assertTrue($this->object->getField('name')->isModified());
        
        $this->object->set('id', 1);
        $this->assertEquals(null, $this->object->getField('id')->getOriginValue());
        $this->assertEquals(1, $this->object->getField('id')->getValue());
        $this->assertTrue($this->object->getField('name')->isModified());
        
        $this->object->set('prp.test', 'ok');
        $this->object->save();
    }
    
    public function testSaveUpdateNoData()
    {
        $this->object->getDataManager()->setListener('first', function($resource, $pkey) {
            return ['id'=>'1', 'name'=>'test'];
        });
        
        $this->object->load(['id'=>'1']);
        
        $this->assertEquals('1', $this->object->get('id'));
        $this->assertEquals('test', $this->object->get('name'));
        
        // salvataggio senza modificare i dati
        $this->object->getDataManager()->setListener('update', function($resource, $data, $pkey) {
            $this->fail('Dati da salvare : '.json_encode($data) );
        });
        // il salvataggio non verrà effettuato perchè i dati non sono stati modificati
        $this->object->save();
    }
    
    /**
     * @expectedException \Exception
     * @expectedExceptionMessage OK UPDATE
     */
    public function testSaveUpdateWhithData()
    {
        $this->object->getDataManager()->setListener('first', function($resource, $pkey) {
            return ['id'=>'1', 'name'=>'test'];
        });
        
        $this->object->load(['id'=>'1']);
        $this->assertEquals('1', $this->object->getField('id')->getValue());
        $this->assertEquals('1', $this->object->getField('id')->getOriginValue());
        // salvataggio modificando un campo
        $this->object->getDataManager()->setListener('update', function($resource, $data, $pkey) {
            $this->assertEquals('TestTable', $resource['name']);
            $this->assertEquals(['id'=>1], $pkey);
            $this->assertArraySubset(['name'=>'prova'], (array) $data);
            throw new \Exception('OK UPDATE');
        });
        $this->assertEquals('1', $this->object->get('id'));
        $this->object->set('name', 'prova');
        $this->assertEquals('prova', $this->object->get('name'));
        $this->object->save();
    }
    
    
    public function testDataJson()
    {
        $this->object->getDataManager()->setListener('first', function($resource, $pkey) {
            return $pkey;
        });
        
        $this->object->getDataManager()->setListener('update', function($resource, $data, $pkey) {
            $this->assertEquals(['id'=>1, 'name'=>'test', 'prp'=>'[]'], (array) $data);
            $this->assertEquals(['id'=>1], (array) $pkey);
        });
        
        $this->object->set('name', 'test');
        $this->object->set('id', 1);
        $this->object->set('prp', null);
        
        $this->object->save();
        
        $this->object->getDataManager()->setListener('update', function($resource, $data, $pkey) {
            $this->assertEquals(['id'=>1, 'name'=>'test', 'prp'=>'{"test":"ok"}'], (array) $data);
            $this->assertEquals(['id'=>1], (array) $pkey);
        });
        
        $this->object->set('prp.test', 'ok');
        $this->object->save();
        
    }
}

class AdapterMock implements ModelAdapterInterface
{
    use \Spinit\Util\TriggerTrait;
    private $param = ['resource'=>'TestTable'];
    
    public function init(Model $model)
    {
        $model->map('id', 'increment')->setPkey();
        $model->map('name', 'unknow');
        $model->map('prp', 'json');
    }

    public function getParam($name)
    {
        return Util\arrayGet($this->param, $name);
    }
    public function setParam($name, $value)
    {
        $this->param[$name] = $value;
        return $this;
    }

    public function getInfo() {
        
    }

    public function getInitRecords() {
        return [];
    }

}

class DataManagerMock implements DataManagerInterface
{
    use \Spinit\Util\TriggerTrait;
    
    private $listener = [];
    
    public function setListener($type, $callable)
    {
        $this->listener[$type] = $callable;
    }
    public function align(DataStructInterface $struct)
    {
        return call_user_func(Util\arrayGetAssert($this->listener, 'align'), $struct);
    }
    public function check($name)
    {
        return call_user_func(Util\arrayGetAssert($this->listener, 'check'), $name);
    }
    
    public function exec($query, $param = array(), $args = array())
    {
        return call_user_func(Util\arrayGetAssert($this->listener, 'exec'), $query, $param, $args);
    }
    public function load($query, $param = array(), $args = array())
    {
        return call_user_func(Util\arrayGetAssert($this->listener, 'load'), $query, $param, $args);
    }

    public function first($resource, $pkey, $fields = '')
    {
        return call_user_func(Util\arrayGetAssert($this->listener, 'first'), $resource, $pkey, $fields);
    }
    public function find($resource, $fields, $pkey)
    {
        return call_user_func(Util\arrayGetAssert($this->listener, 'find'), $resource, $fields, $pkey);
    }
    public function insert($resource, $data)
    {
        return call_user_func(Util\arrayGetAssert($this->listener, 'insert'), $resource, $data);
    }
    public function update($resource, $data, $pkey)
    {
        return call_user_func(Util\arrayGetAssert($this->listener, 'update'), $resource, $data, $pkey);
    }
    public function delete($resource, $pkey)
    {
        return call_user_func(Util\arrayGetAssert($this->listener, 'delete'), $resource, $pkey);
    }

    public function trace($origin, $data)
    {
        return call_user_func(Util\arrayGetAssert($this->listener, 'trace'), $origin, $data);
    }
    
    public function getSchema() {
        
    }

    public function setSchema($name) {
        
    }

    public function drop($name) {
        
    }

}
