<?php
namespace Spinit\Core\Model\TestUnit\Adapter\Xml;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Spinit\Core\Model\Adapter\Xml\ModelAdapterXml;
use Spinit\Core\Model\Model;

/**
 * Description of ModelAdampterXmlTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ModelAdampterXmlTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     * @var ModelAdapterXml
     */
    private $object;
    
    public function setup()
    {
        $code = <<< EOCODE
    <model resource="test" property="testPrp">
        <!-- Struttura della risorsa principale -->
        <struct>
            <!-- Campo della risorsa principale -->
            <field name="field1"/>
        </struct>
        <init>
            <record>
                <field name="field1" apply="md5">hello</field>
            </record>
        </init>
    </model>
EOCODE;
        $this->object = new ModelAdapterXml($code, '');
    }
    
    public function testType()
    {
        $model = new Model($this->object);
        $this->assertNotFalse($model->has('field1'));
    }
    
    public function testRecord()
    {
        foreach($this->object->getInitRecords() as $rec) {
            $this->assertEquals(['field1' => md5('hello')], $rec);
            break;
        }
    }
    
}
