<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model\TestUnit;

use PHPUnit\Framework\TestCase;
use Spinit\Core\Model\Field;
use Spinit\Core\Model\FieldType\ValueType;
/**
 * Description of FieldTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class FieldTest extends TestCase
{
    private $object;
    
    public function setUp()
    {
        $this->object = new Field($this, 'test', new ValueType());
    }
    
    public function testUse()
    {
        $this->assertEquals('test', $this->object->getName());
        $this->assertFalse($this->object->hasValue());
        $this->object->setValue('ok');
        $this->assertTrue($this->object->hasValue());
        $this->assertEquals('ok', $this->object->getValue());
        $this->assertNotNull($this->object->getType());
        
        $this->assertEquals(false, $this->object->getPkey());
        $this->assertEquals(false, $this->object->getNotNull());
        $this->assertEquals(null, $this->object->getDefault());
        
    }
    
    /**
     * @expectedException \Exception
     */
    public function testError()
    {
        $this->assertEquals('test', $this->object->getName());
        $this->assertFalse($this->object->hasValue());
        $this->object->getValue();
    }
}
