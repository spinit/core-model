<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model\Interfaces;

use Spinit\Core\Model\Model;
use Spinit\Util\TriggerInterface;

/**
 * Description of Adapter
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface ModelAdapterInterface extends TriggerInterface
{
    public function init(Model $model);
    public function getParam($name);
    public function getInfo();
    public function getInitRecords();
}
