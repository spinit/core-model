<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model\Interfaces;

use Spinit\Datamanager\DataManagerInterface;

/**
 * Description of Model
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

interface FieldTypeInterface
{
    public function format($value, $opt);
    public function check($value, $opt, $oldValue);
    public function serialize(DataManagerInterface $manager, $value, $field);
    public function getTypeName();
}
