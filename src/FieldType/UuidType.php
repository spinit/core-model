<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model\FieldType;
use Spinit\Datamanager\DataManagerInterface;

/**
 * Description of IncrementType
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class UuidType extends ValueType
{
    public function check($value, $opt, $oldValue)
    {
        if ($value === '') {
            return null;
        }
        return parent::check($value, $opt, $oldValue);
    }
    public function serialize(DataManagerInterface $manager, $value, $field)
    {
        if (!$value and $field->isPkey()) {
            return (string) $manager->getCounter()->next();
        }
        return parent::serialize($manager, $value, $field);
    }
    public function getTypeName()
    {
        return 'uuid';
    }
}
