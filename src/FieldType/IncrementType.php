<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model\FieldType;

/**
 * Description of IncrementType
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class IncrementType extends ValueType
{
    public function check($value, $opt, $oldValue)
    {
        if ($value === '') {
            return null;
        }
        return parent::check($value, $opt, $oldValue);
    }
    public function getTypeName()
    {
        return 'increment';
    }
}
