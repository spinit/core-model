<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model\FieldType;

use Spinit\Core\Model\FieldType;;
use Spinit\Datamanager\DataManagerInterface;

/**
 * Description of Model
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class ValueType extends FieldType
{
    public function format($value, $opt)
    {
        return $value;
    }

    public function check($value, $opt, $oldValue)
    {
        return $value;
    }
    public function serialize(DataManagerInterface $manager, $value, $field)
    {
        switch($field->get('type')) {
            case 'integer':
            case 'int':
                if ($value === '') {
                    return null;
                }
        }
        return $value;
    }
    public function getTypeName()
    {
        return '';
    }
}
