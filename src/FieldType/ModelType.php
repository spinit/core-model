<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model\FieldType;

use Spinit\Core\Model\FieldType;
use Spinit\Datamanager\DataManagerInterface;
/**
 * Description of ModelType
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ModelType extends FieldType
{
    
    public function getTypeName()
    {
        return "model";
    }

    public function serialize(DataManagerInterface $manager, $value)
    {
        
    }

}
