<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model\FieldType;
use Spinit\Datamanager\DataManagerInterface;
use Spinit\Util\Dictionary;

/**
 * Description of IncrementType
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class JsonType extends ValueType
{
    public function check($value, $opt, $oldValue)
    {
        $jdata = json_encode($oldValue, 1);
        if ($opt) {
            if (is_string($oldValue)) {
                $oldValue = json_decode($oldValue, 1);
            }
            if (!$oldValue) {
                $oldValue = [];
            };
            if ($oldValue instanceof Dictionary) {
                $data = $oldValue;
            } else {
                $data = new Dictionary($oldValue);
            }
            $data->set($opt, $value);
        } else {
            if (is_string($value)) {
                $value = json_decode($value, 1);
            }
            if (!$value) {
                $value = [];
            };
            if ($value instanceof Dictionary) {
                $data = $value;
            } else {
                $data = new Dictionary($value);
            }
        }
        return $data;
    }
    public function format($value, $opt) {
        if (is_string($value)) {
            $value = json_decode($value, 1);
        }
        if (!$value) {
            $value = [];
        };
        if ($opt) {
            if ($value instanceof Dictionary) {
                $data = $value;
            } else {
                $data = new Dictionary($value);
            }
            return $data->get($opt);
        }
        return $value;
    }
    public function serialize(DataManagerInterface $manager, $value, $field)
    {
        return json_encode($value);
    }
    public function getTypeName()
    {
        return 'json';
    }
}
