<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model;

use Spinit\Core\Model\Interfaces\FieldTypeInterface;
use Webmozart\Assert\Assert;

use Spinit\Datastruct\Field as DataStructField;

/**
 * Description of Field
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Field extends DataStructField
{
    private $name;
    private $type;
    private $value;
    private $origin;
    
    private $hasValue;
    
    private $model;
    
    use \Spinit\Util\TriggerTrait;
    
    public function __construct($model, $name, FieldTypeInterface $type)
    {
        parent::__construct($name);
        $this->model = $model;
        $this->name = $name;
        $this->type = $type;
        $this->clear();
        
        $this->setNotNull(false);
        $this->setDefault(null);
        $this->setPkey(false);
    }
    
    public function getModel()
    {
        return $this->model;
    }
    public function getName()
    {
        return $this->name;
    }
    
    public function clear()
    {
        $this->value = null;
        $this->origin = null;
        $this->hasValue = false;
    }
    public function getType()
    {
        return $this->type;
    }
    
    public function setValue($value, $opt = null)
    {
        $this->value = $this->getType()->check($value, $opt, $this->value);
        $this->hasValue = true;
        return $this;
    }
    
    public function hasValue()
    {
        return $this->hasValue;
    }
    
    public function getValue()
    {
        $args = func_get_args();
        if (count($args)) {
            return $this->hasValue ? $this->value : $args[0];
        }
        Assert::true($this->hasValue,'Valore non impostato per : '.$this->name);
        return $this->value;
    }
    public function storeValue()
    {
        $value = $this->getType()->serialize($this->getModel()->getDataManager(), $this->value, $this);
        return is_array($value) ?
            json_encode($value, JSON_PRETTY_PRINT| JSON_UNESCAPED_UNICODE) : 
            $value;
    }
    public function setOriginValue($value)
    {
        $this->setValue($value);
        $this->origin = $value;
        return $this;
    }
    public function getOriginValue()
    {
        return $this->origin;
    }
    public function isModified()
    {
        return $this->getOriginValue() != $this->value;
    }
    
    public function setNotNull($is = true)
    {
        $this->set('notnull', $is);
        return $this;
    }
    
    public function getNotNull()
    {
        return $this->get('notnull');
    }
    
    public function setDefault($default)
    {
        $this->set('default', $default);
        return $this;
    }
    
    public function getDefault()
    {
        return $this->get('default');
    }
    
    public function setPkey($pKey = true)
    {
        $this->set('ispkey', $pKey);
        $this->trigger($pKey ? 'setPkey' : 'unsetPkey');
        return $this;
    }
    
    public function getPkey()
    {
        return $this->get('ispkey', false);
    }
    
    public function isPkey()
    {
        $args = func_get_args();
        if (count($args)) {
            return $this->setPkey($args[0]);
        }
        return $this->getPkey();
    }
}