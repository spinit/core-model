<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model\Adapter\Xml;

use Spinit\Core\Model\Interfaces\ModelAdapterInterface;
use Spinit\Core\Model\Model;
use Spinit\Util;
/**
 * Description of ModelAdapterXml
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ModelAdapterXml implements ModelAdapterInterface
{
    use Util\TriggerTrait;
    
    private $filename;
    private $index;
    private $trigger = [];
    
    public function __construct($modelxml, $type = 'filename')
    {
        $this->filename = '';
        switch($type) {
            case 'filename':
                $this->filename = $modelxml;
                $this->index = simplexml_load_file($this->filename, null, LIBXML_NOCDATA);
                break;
            case '':
                $this->index = simplexml_load_string($modelxml, null, LIBXML_NOCDATA);
                break;
        }
        foreach(Util\nvl($this->index->trigger->event, []) as $event) {
            foreach(Util\nvl($event->children(), []) as $code) {
                $this->trigger[(string)$event['name']][(string)$event['when']][] = $code;
            }
        }
    }
    
    public function getParam($name)
    {
        return (string) $this->index[$name];
    }

    public function init(Model $model)
    {
        foreach($this->index->struct->field ?:[] as $field) {
            $item = $model->map((string) $field['name'], (string) $field['type'])
                ->set('apply', (string) $field['apply'])
                ->set('size', (string) $field['size'])
                ->isPkey(!!$field['pkey'])
                ->setNotNull(!!$field['notnull'])
                ->setValue((string) $field['default']);
        }
        foreach($this->index->struct->property ?:[] as $field) {
            
        }
    }

    public function getInfo()
    {
        return $this->filename;
    }

    public function getInitRecords()
    {
        if ($this->index->init) {
            foreach($this->index->init->record  as $record) {
                $rec = [];
                foreach($record->field as $child) {
                    $val = (string) $child;
                    if ($apply = (string)$child['apply']) {
                        $val = call_user_func($apply, $val);
                    }
                    $rec[(string) $child['name']] = $val;
                }
                yield $rec;
            }
        }
    }

    public function onTrigger($event, $target)
    {
        foreach(Util\arrayGet(Util\arrayGet($this->trigger, $event['name']), $event['when'], []) as $code) {
            $this->process($code, $target);
        }
    }
    
    private function process($item, $target)
    {
        switch($item->getName()) {
            case 'set' :
                $target->set((string) $item['field'], $target->normalize($item));
                break;
        }
    }
    
}
