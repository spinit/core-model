<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model;

use Spinit\Core\Model\Interfaces\FieldTypeInterface;
use Spinit\Core\Model\Model;
use Webmozart\Assert\Assert;

/**
 * Description of FieldType
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

abstract class FieldType implements FieldTypeInterface
{
    /**
     *
     * @var Model
     */
    private $model = null;
    
    public function __construct(Model $model = null)
    {
        $this->model = $model;
    }
    
    public function getModel()
    {
        Assert::notNull($this->model);
        return $this->model;
    }
}
