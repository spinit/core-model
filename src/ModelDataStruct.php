<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model;

use Spinit\Datastruct\DataStructInterface;
use Spinit\Util\TriggerInterface;
/**
 * Description of ModelDataStruct
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ModelDataStruct implements DataStructInterface
{
    public function __construct($name, $pkey, $fieldList, $indexList, $param)
    {
        
    }
    
    public function addPkeyField($fieldName) {
        
    }

    public function execDiff(DataStructInterface $otherStruct, TriggerInterface $listener = null) {
        
    }

    public function getField($name) {
        
    }

    public function getFieldList() {
        
    }

    public function getIndex($name) {
        
    }

    public function getIndexList() {
        
    }

    public function getName() {
        
    }

    public function getParam($name) {
        
    }

    public function getPkey() {
        
    }

}
