<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model;

use Spinit\Core\Model\Interfaces\ModelAdapterInterface;
use Spinit\Core\Model\Interfaces\FieldTypeInterface;
use Spinit\Util;
use Spinit\Datamanager\DataManagerInterface;
use Spinit\Datastruct\DataStruct;
use Spinit\Core\Model\Interfaces\IsModelInterface;

use Webmozart\Assert\Assert;
use Spinit\Util\Error\NotFoundException;

/**
 * Description of Model
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class Model implements IsModelInterface
{
    use Util\ParamTrait;
    use Util\TriggerTrait;
    
    private $adapter;
    private $fields;
    private $pkeys;
    private $manager;
    // ultimo evento importante inviato
    private $event;
    public $debug;
    /**
     *
     * @var DataStruct
     */
    private $datastruct = null;
    private $modelFactory = null;
    
    private $types = [
        'uuid' => 'Spinit:Core:Model:FieldType:UuidType',
        'json' => 'Spinit:Core:Model:FieldType:JsonType',
        'increment' => 'Spinit:Core:Model:FieldType:IncrementType',
        'model' => 'Spinit:Core:Model:FieldType:ModelType'
    ];
    
    private $property;
    /**
     * 
     * @param ModelAdapterInterface $adapter
     * @param type $manager
     * @param type $modelFactorty
     */
    public function __construct(ModelAdapterInterface $adapter, $manager = null, $modelFactorty = null)
    {
        $manager        && $this->setDataManager($manager);
        $modelFactorty  && $this->setModelFactory($modelFactorty);
        
        /*
        $this->bindExec('load', function($event) {return $this->getAdapter()->trigger('load',[$event->getParam(0)]);});
        $this->bindExec('save', function() {return $this->getAdapter()->trigger('save');});
        $this->bindExec('delete', function() {return $this->getAdapter()->trigger('delete');});
        $this->bindExec('exec', function() {return $this->getAdapter()->trigger('exec');});
        
         * 
         */
        $this->bindExec('insert', function($event) {
            $this->getDataManager()->insert($event->getParam(0), $event->getParam(1), false, function() {
                $this->trigger('@insert', func_get_args());
            });

        });
        $this->bindExec('update', function($event) {
            $this->getDataManager()->update($event->getParam(0), $event->getParam(1), $event->getParam(2), false, function() {
                $this->trigger('@update', func_get_args());
            });

        });
        $this->setAdapter($adapter);
        
        $this->bindExec('load', function($event) {return $this->loadExec($event->getParam(0));});
        $this->bindExec('save', function() {return $this->saveExec();});
        $this->bindExec('delete', function() {return $this->deleteExec();});
        $this->bindExec('exec', function() {return $this->execExec();});
        
        $this->trigger(['log','info'], [$this->getInfoResource()]);
        
    }
    
    protected function onTrigger($event, $target)
    {
        $this->getAdapter()->execTrigger($event, $this);
    }
    public function getDataArray()
    {
        $data = [];
        foreach($this->getFieldList() as $name => $field) {
            if (!($field instanceof Field)) {
                $data [$name] = $field->get('value', '');
                continue;
            } 
            $data[$field->getName()] = $field->storeValue();
        }
        return $data;
    }
    /**
     * Il modelFactory viene richiamato per poter caricare il modello a cui un campo si riferisce
     */
    public function setModelFactory($caller)
    {
        $this->modelFactory = $caller;
    }
    
    public function getResource()
    {
        $resource = $this->datastruct->getName();
        Assert::notEmpty($resource, "Name resource required in ". $this->getAdapter()->getInfo());
        return $resource;
    }
    
    /**
     * Il modello chiede all'adapter di configurarne la struttura
     * @param ModelAdapterInterface $adapter
     */
    private function setAdapter(ModelAdapterInterface $adapter)
    {
        $this->fields = [];
        $this->pkeys = [];
        $this->adapter = $adapter;
        $this->datastruct = new DataStruct($this->adapter->getParam('resource'));
        $this->datastruct->setParam('trace', $this->getAdapter()->getParam('trace'));
        // quando verrà creata la tabella allora verranno inseriti i valori di default
        $this->datastruct->bindExec('createTable', function() {
            return $this->addInitRecord();
        });
        $this->adapter->init($this);
        $this->property = new ModelProperty($this);
    }

    public function getAdapter()
    {
        return $this->adapter;
    }
    
    /**
     * Il modello utilizzerà il DataManager come Bridge per aggiornare la fonte dati
     * @param DataManagerInterface|callable $manager
     */
    public function setDataManager($manager)
    {
        $this->manager = $manager;
    }
    
    public function getDataManager()
    {
        Assert::notNull($this->manager);
        if ($this->manager instanceof DataManagerInterface) {
            return $this->manager;
        }
        if (is_callable($this->manager)) {
            return call_user_func($this->manager, $this->getAdapter()->getParam('datasource'));
        }
        throw new \Exception('Data manager sconosciuto : '.get_class($this->manager));
    }
    
    /**
     * Aggiorna la struttura presente sul DataManager con quella attualmente caricata
     */
    public function init()
    {
        $this->getDataManager()->align($this->datastruct);
        return $this;
    }
    /**
     * Funzione richiamata dalla struct in caso di creazione della tabella.
     * Permette di poter inserire la lista di record predefiniti
     */
    private function addInitRecord()
    {
        foreach($this->getAdapter()->getInitRecords() as $record) {
            $this->clear();
            $this->set($record);
            $this->save();
        }
    }
    public function getField($name)
    {
        $part = explode('.', $name);
        return $this->datastruct->getField(array_shift($part));
        //return Util\arrayGetAssert($this->fields, $name);
    }
    
    public function getFieldList()
    {
        return $this->datastruct->getFieldList();
        //return $this->fields;
    }
    
    public function clear()
    {
        foreach($this->datastruct->getFieldList() as $field) {
            $field->clear();
        }
        $this->property->clear();
        $this->pkey = [];
        return $this;
    }
    public function getPkey()
    {
        $pkey = [];
        try {
            foreach($this->datastruct->getPkey() as $pk) {
                $pkey[$pk] = $this->get($pk);
            }
        } catch (\Exception $e) {
            return null;
        }
        return $pkey;
    }
    
    public function mapType($name, $class)
    {
        $this->types[$name] = $class;
        return $this;
    }
    
    public function map($name, $type)
    {
        $fieldType = Util\getInstance(Util\arrayGet($this->types, $type, 'Spinit:Core:Model:FieldType:ValueType'), $this);
        
        $field = $this->datastruct->addField(new Field($this, $name, $fieldType));
        $field->set('type', $type);
        $field->bindExec('setPkey', function() use ($name) {
            $this->datastruct->addPkeyField($name);
        });
        $field->bindExec('unsetPkey', function() use ($name) {
            $this->datastruct->removePkeyField($name);
        });
        return $field;
    }
    
    public function set($name)
    {
        $args = func_get_args();
        if (count($args)>1) {
            $opt = array_filter(explode('.', $name), 'strlen');
            $this->getField(array_shift($opt))->setValue($args[1], implode('.', $opt));
            return $this;
        }
        foreach($name?:[] as $k => $v) {
            $this->set($k, $v);
        }
        return $this;
    }
    
    public function get($name)
    {
        $args = func_get_args();
        $opt = array_filter(explode('.', $name), 'strlen');
        $field = $this->getField(array_shift($opt));
        if (count($args)>1) {
            $value = $field->getValue($args[1]);
        } else {
            $value = $field->getValue();
        }
        return $field->getType()->format($value, implode('.', $opt));
    }
    
    public function has($name)
    {
        try {
            return $this->datastruct->getField($name);
        } catch (\Exception $ex) {
            return false;
        }
    }
    
    public function load($pkey)
    {
        $this->clear();
        return $this->trigger('load', [$pkey]);
    }
    
    public function save($fnc = null)
    {
        $result = $this->trigger('save');
        if ($fnc and is_callable($fnc)) {
            $fnc($this->event);
        }
        return $result;
    }
    
    public function delete()
    {
        return $this->trigger('delete');
    }
    
    private function loadExec($pkey)
    {
        $data = $this->getDataManager()->first($this->getResource(), $pkey)?:[];
        foreach($data as $name => $value) {
            try {
                $field = $this->getField($name);
                if ($field instanceof Field) {
                    $field->setOriginValue($value);
                } else {
                    $field->set('value', $value);
                }
            } catch (NotFoundException $e) {
                // nella struttura sono presenti campi non presenti nel model (tipo dat_ins__)
            }
        }
    }
    private function getInfoResource()
    {
        return [
            'name'=>$this->getResource(),
            'trace'=>$this->getAdapter()->getParam('trace'),
            'user'=>$this->getParam('user')
        ];
    }
    private function saveExec()
    {
        $data = $pkey = $ppkey = [];
        foreach($this->getFieldList() as $name => $field) {
            
            if (!($field instanceof Field)) {
                $data [$name] = $field->get('value', '');
                continue;
            } 
            if ($field->isPkey()) {
                $pkey [$name] = $field->getOriginValue();
                $ppkey[$name] = $field->storeValue();
            }
            if ($field->isModified() or $field->isPkey()) {
                $data [$name] = $field->storeValue();
            }
            if (isset($data[$name]) and $apply = $field->get('apply', '')) {
                $data[$name] = [$apply, $data[$name]];
            }
        }
        // se ha una chiave con la quale sono stati letti i dati
        $this->event = '';
        if (count($data) and $data != $pkey) {
            $strpkey = trim(implode('', $pkey));
            if (strlen($strpkey) == 0) {
                $pkey = $this->checkPkey($ppkey);
                $strpkey = trim(implode('', $pkey));
            }
            if (strlen($strpkey) > 0) {
                $this->event = 'update';
                $this->trigger($this->event, [$this->getInfoResource(), new \ArrayObject($data), $pkey]);
            } else {
                $this->event = 'insert';
                $this->trigger($this->event, [$this->getInfoResource(), new \ArrayObject($data)]);
            }
            foreach($pkey as $pk=>$val) {
                $this->set($pk, $data[$pk]);
            }
        }
        $this->property->sync();
        return $this->event;
    }
    
    private function checkPkey($pkey)
    {
        $rec = $this->getDataManager()->first($this->getResource(), $pkey)?:[];
        $ppkey = [];
        foreach(array_keys($pkey) as $fld) {
            $ppkey[$fld] = Util\arrayGet($rec, $fld);
        }
        return $ppkey;
    }
    private function deleteExec()
    {
        $data = $pkey = [];
        
        foreach($this->getFieldList() as $name => $field) {
            
            if (!($field instanceof Field)) {
                continue;
            } 
            if ($field->isPkey()) {
                $pkey [$name] = $field->getValue();
            }
        }
        
        // se ha una chiave con la quale sono stati letti i dati
        if (trim(implode('', $pkey))) {
            $soft = false;
            $this->getDataManager()->delete($this->getInfoResource(), $pkey, $soft, function() {
                $this->trigger('@delete', func_get_args());
            });
            if (!$soft) {
                $this->property->delete();
            }
        }
    }
    
    public function setProperty($pid, $val, $type = 'string')
    {
        $this->property->setValues($pid, $val, $type);
        return $this;
    }
    
    public function getProperty($pid, $type = 'string')
    {
        return $this->property->getValues($pid, $type);
    }
    public function normalize($string)
    {
        $string = (string) $string;
        preg_match_all('/\{\{([^ ]+)\}\}/', $string, $matches);
        foreach ($matches[1] as $k => $name) {
            $string = str_replace($matches[0][$k], $this->get($name, ''), $string);
        }
        return trim($string);
    }
}
