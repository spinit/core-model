<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model;

use Spinit\Util;

use Spinit\Datastruct\DataStruct;
use Spinit\Datastruct\Field as stField;

/**
 * Description of ModelProperty
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ModelProperty
{
    /**
     *
     * @var Model
     */
    private $model;
    private $values;
    private $trace = '';
    
    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->values = false;
        $this->property = $model->getResource().'__prp';
        $this->trace = $this->property.'x';
    }
    
    public function clear()
    {
        $this->values = false;
    }
    
    private function align()
    {
        if (!$this->model->getDataManager()->check($this->property)) {
            $this->model->getDataManager()->align($this->getPropertyDataStruct());
            $this->model->getDataManager()->align($this->getTraceDataStruct());
        }
    }
        
    private function getPropertyDataStruct()
    {
        $ds = new DataStruct($this->property);
        $ds->addField(new stField('id', ['type'=>'uuid', 'pkey'=>'1']));
        $ds->addField(new stField('id_mst', ['type'=>'uuid']));
        $ds->addField(new stField('id_prp', ['type'=>'uuid']));
        $ds->addField(new stField('typ', ['type'=>'integer']));
        $ds->addField(new stField('val_uid', ['type'=>'uuid']));
        $ds->addField(new stField('val_str', ['type'=>'text']));
        $ds->addField(new stField('usr_ins__', ['type'=>'uuid']));
        $ds->addField(new stField('dat_ins__', ['type'=>'datetime']));
        return $ds;
    }
        
    private function getTraceDataStruct()
    {
        $ds = new DataStruct($this->trace);
        $ds->addField(new stField('id', ['type'=>'uuid', 'pkey'=>'1']));
        $ds->addField(new stField('id_mst', ['type'=>'uuid']));
        $ds->addField(new stField('id_prp', ['type'=>'uuid']));
        $ds->addField(new stField('typ', ['type'=>'integer']));
        $ds->addField(new stField('val_uid', ['type'=>'uuid']));
        $ds->addField(new stField('val_str', ['type'=>'text']));
        $ds->addField(new stField('usr_ins__', ['type'=>'uuid']));
        $ds->addField(new stField('dat_ins__', ['type'=>'datetime']));
        $ds->addField(new stField('usr_opr__', ['type'=>'uuid']));
        $ds->addField(new stField('dat_opr__', ['type'=>'datetime']));
        return $ds;
    }
    
    public function setValues($pid, $values, $type = '')
    {
        if (!is_array($values)) {
            $values = [$values];
        }
        $this->values[strtoupper(md5($pid))][Util\nvl($type, 'string')] = $values;
    }
    
    public function getValues($pid, $type = '')
    {
        $this->align();
        $pval = $this->getPVal();
        if (!is_array($this->values)) {
            foreach($this->model->getDataManager()->find($this->property, 'typ, id_prp, val_uid, val_str', ['id_mst' => $pval]) as $prp) {
                switch($prp['typ']) {
                    case 1:
                        $this->values[strtoupper($prp['id_prp'])]['uuid'][] = $prp['val_uid'];
                        break;
                    default:
                        $this->values[strtoupper($prp['id_prp'])]['string'][] = $prp['val_str'];
                        break;
                }
            }
        }
        return Util\arrayGet($this->values, [strtoupper(md5($pid)), Util\nvl($type, 'string')], []);
    }
    
   
    public function sync()
    {
        // se i valori non sono inizializzati ... allora non viene fatta nessuna operazione
        if ($this->values === false) {
            return;
        }
        $this->align();
        
        $prps = $this->loadIdMap();
        // aggiorna e inserisce i valori non presenti nel DB
        $this->storeValues($prps);
        // toglie i valori non più necessari
        $this->deleteValues($prps);
    }
    
    private function loadIdMap()
    {
        $prps = new \ArrayObject();
        $pval = $this->getPVal();
        // proprietà effettive
        foreach($this->model->getDataManager()->find($this->property, '*', ['id_mst'=>$pval]) as $prp) {
            switch($prp['typ']) {
                case 1:
                    $prps[strtoupper($prp['id_prp'])]['uuid'][$prp['val_uid']] = $prp['id'];
                    break;
                default:
                    $prps[strtoupper($prp['id_prp'])]['string'][$prp['val_str']] = $prp['id'];
                    break;
            }
        }
        return $prps;
    }
    
    private function getFieldType($name)
    {
        switch ($name) {
            case 'uuid':
                return ['val_uid', 1];
            default : 
                return ['val_str', 0];
        }
    }
    private function storeValues($prps)
    {
        $pval = $this->getPVal();
        // proprietà da memorizzare
        foreach($this->values as $id_prp => $listPrp) {
            foreach($listPrp as $typ_prp => $listValue) {
                foreach($listValue as $value) {
                    if (isset($prps[$id_prp][$typ_prp][$value])) {
                        unset($prps[$id_prp][$typ_prp][$value]);
                    } else {
                        list($field, $typ) = $this->getFieldType($typ_prp);
                        $this->model->getDataManager()->insert(
                            $this->property, 
                            [
                                'id' => $this->model->getDataManager()->getCounter()->next(),
                                'id_mst' => $pval,
                                'id_prp'=>$id_prp,
                                'typ'=>$typ,
                                $field => $value,
                                'usr_ins__'=>$this->model->getDataManager()->getParam('userTrace', null),
                                'dat_ins__'=>date('Y-m-d H:i:s')
                            ]
                        );
                    }
                }
            }
        }
    }
    
    private function deleteValues($prps)
    {
        // proprietà da cancellare
        foreach($prps as $listPrp) {
            foreach($listPrp as $listValue) {
                foreach($listValue as $value => $pid) {
                    $this->model->getDataManager()->exec("
                        insert into {$this->trace}
                              (id, id_mst, id_prp, typ, val_uid, val_str, dat_ins__, usr_ins__,  dat_opr__, usr_opr__)
                        select id, id_mst, id_prp, typ, val_uid, val_str, dat_ins__, usr_ins__, now(), unhex({{user}})
                        from {$this->property}
                        where id = unhex({{pid}})", ['user'=>$this->model->getDataManager()->getParam('userTrace', null), 'pid'=>$pid]);
                    $this->model->getDataManager()->delete($this->property, ['id'=>$pid]);
                }
            }
        }
    }
    
    public function delete()
    {
        $pval = $this->getPVal();
        if ($this->model->getDataManager()->check($this->property)) {
            $this->model->getDataManager()->delete($this->property,['id_mst' => $pval]);
            $this->model->getDataManager()->delete($this->trace,['id_mst' => $pval]);
        }
    }
    
    private function getPVal()
    {
        $pkey = $this->model->getPkey() ?: [];
        return array_shift($pkey);
    }
}
