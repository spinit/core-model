<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Core\Model;

use Spinit\Core\Model\Interfaces\CommandInterface;
use Spinit\Core\Model\Model;

/**
 * Description of Command
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class Command implements CommandInterface
{
    /**
     *
     * @var Model
     */
    private $model;
    
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
    
    /**
     * 
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }
}
